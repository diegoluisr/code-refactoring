<?php

namespace App\Http\Controllers;

use App\Http\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

// Asumimos que esta clase hace parte de una arquitectura que aplica "principio de single responsibility"
// debe existir funcionalidades de orde superior que limitan el acceso a usuarios no autorizados a ciertos recursos
// ya sea por medio de ACL (Access Control Lists) o RBAC (Role-based Access Control)
// De igual forma deberia existir un manejador que llama la funcion adecuada de un controlador dependiendo el
// metodo de solicitud HTTP

class ServicioController extends Controller
{
    public function patch(Request $request, Response $response)
    {
        // por seguridad los metodos que insertan o modifican datos en una transaccion deben ir en formato
        // application/x-www-form-urlencoded, form-data, raw o binary
        // el uso de input permite que los datos sean enviados por metodo GET siendo valido una peticion
        // api/endpoint/?service_id=1&driver_id=2
        // Personalmente basado en el modelo de madurez de Richardson
        // https://martinfowler.com/articles/richardsonMaturityModel.html
        // este metodo deberia ser solo habilitado bajo el Verbo / Metodo PATCH puesto que solo se esta actualizando un campo

        // obtenemos los datos de la peticion y filtramos los datos
        // https://laravel.com/docs/5.2/requests
        // Laravel se encarga de darnos acceso a los datos ya sean si estan crudos o no por medio del metodo "input"
        // http://php.net/manual/es/function.filter-var.php

        $service_id = filter_var($request->input('service_id'), FILTER_VALIDATE_INT, ['min_range' => 1]);
        $driver_id = filter_var($request->input('driver_id'), FILTER_VALIDATE_INT, ['min_range' => 1]);

        // obtenemos y validamos los datos del driver de forma anticipada, antes de realizar operaciones contra base de datos
        // para mejorar el rendimiento
        if($service_id === false || $driver_id === false)
        {
            // respondemos adecuadamente indicando que los datos no estan completo o no cumpler el filtrado
            return $response->json(['data' => '', 'error'=> trans('messages.invalid_arguments')], 400);
        }

        // con el id filtrado tenemos certeza que es un entero y no una cadena de texto que podria dejar las puertas 
        // abiertas para SQL Injection
        $servicio = Service::find($service_id);
        if($servicio == null)
        {
            // asuminos que existe un mensaje preconfigurado del tipo
            // 'record_not_exists' => 'El registro de tipo :entity al cual quiere acceder no existe (ID :id)'
            return $response->json(['data' => '', 'error'=> trans('messages.record_not_exists', ['entity' => 'Service', 'id' => $service_id])], 404);
        }

        // El status_id = 6 corresponde a un servicio cuyo estado no es valido para continuar
        // Los estatus deberian ser constantes facilmente entendibles
        if ($servicio->status_id == Service::DISABLED) // Service::DISABLED = 6
        {
            return $response->json(['data' => '', 'error'=> trans('messages.service_disabled')], 404);
        }

        // El servicio no debe tener conductor y su estado debe ser diferente a 1
        if ($servicio->driver_id != NULL || $servicio->status_id != Service::ALLOWED) // Service::ALLOWED = 1
        {
            return $response->json(['data' => '', 'error'=> trans('messages.service_not_allowed_or_driver_already_assigned')], 404);
        }

        // consultamos el conductor
        $driver = Driver::find($driver_id);
        if($driver == null) {
            return $response->json(['data' => '', 'error'=> trans('messages.record_not_exists', ['entity' => 'Driver', 'id' => $driver_id])], 404);
        }

        // Modificamos los valores del servicio y guaramos
        $servicio->driver_id = $driver->id;
        $servicio->status_id = Service::NOT_ALLOWED; // Service::NOT_ALLOWED = 2
        $servicio->car_id = $driver->car_id; // preguntar

        // Iniciamos la transaccion para en caso de algun fallo realizar rollback
        DB::beginTransaction();
        if(!$servicio->save()) {
            return $response->json(['data' => '', 'error'=> trans('messages.record_not_updated', ['entity' => 'Service', 'id' => $service_id])], 404);
        }

        // Actualizamos al conductor
        $driver->available = Driver::NOT_AVAILABLE; // Driver::NOT_AVAILABLE = 0;
        if(!$driver->save()) {
            DB::rollbackTransaction();
            return $response->json(['data' => '', 'error'=> trans('messages.record_not_updated', ['entity' => 'Driver', 'id' => $driver_id])], 404);
        }

        // Supongamos que los Push Notification los manejamos con un Provider como este
        // https://github.com/davibennun/laravel-push-notification
        // y ya ha sido configurado.
        // se recomienda que se trabaje con Queue de Laravel para evitar problemas de rendimiento
        $push = new Push;
        $push->device_token = $service->user->uuid;
        $push->badge = 1;
        $push->message = $message;
        $push->device_type = $servicio->user->type;
        $push->options = json_encode(json_encode(
            [
                'badge' => 1,
                'sound' => ['android' => 'default', 'ios' => 'honk.wav'],
                'action' => 'Open',
                'service_id' => $service_id;
            ]
        ));

        // guardamos el Push para luego ser procesado por el Queue
        if(!$push->save()) {
            // si el push notification es obligatorio para finalizar la transaccion hacemos Rollback
            DB::rollbackTransaction();
            return $response->json(['data' => '', 'error'=> trans('messages.record_not_exists', ['entity' => 'Push', 'id' => $driver_id])], 404);
        }
        
        DB::commitTransaction();
        return $response->json(['data' => '', 'error'=> trans('messages.service_confirmed')], 200);
    }
}